import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    posts: [],
  },
  getters: {
    posts: state => state.posts,
    totalRows: state => state.posts.length,
    postId : state => Math.max(...state.posts.map(post => parseInt(post.id))) + 1
  },
  mutations: {
    setPost(state, payload){
      state.posts = payload
    },

    addPost(state, payload) {
      state.posts.push(payload)
    },

    deletePost(state, payload) {
      const index = state.posts.findIndex(post => post.id == payload)
      state.posts.splice(index, 1)
    } 
  },
  actions: {
    async getPosts(state) {
      const posts = await Axios.get('https://jsonplaceholder.typicode.com/posts');
      state.commit('setPost', posts.data);      
    },
    
    addPost(state, payload) {
      state.commit('addPost', payload)
    },

    deletePost(state, payload) {
      state.commit('deletePost', payload)
    }
  },
})
